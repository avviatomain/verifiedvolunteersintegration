﻿using System;
using VerifiedVolunteersIntegration.utils;
using System.Web;
using System.Xml;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;

namespace VerifiedVolunteersIntegration.service
{
    /// <summary>
    /// Summary description for VerifiedVolunteersHandler
    /// </summary>

    public class VerifiedVolunteersHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            if (!OnApplicationAuthenticateRequest(context.Request))
            {
                context.Response.StatusCode = 401;
                context.Response.Status = "401 Unauthorized";
                context.Response.Write(Utils.getResponse("You aren't authorized to perform this action"));
                context.Response.End();
            }

            if (HttpContext.Current.Request.HttpMethod != "POST" )
            {
                context.Response.StatusCode = 400;
                context.Response.Status = "400 Bad request";
                return;
            }
            XmlDocument xml = new XmlDocument();
            String content = null;
            context.Response.ContentType = "application/xml";
            try
            {
                using (var reader = new System.IO.StreamReader(context.Request.InputStream))
                {
                    content = reader.ReadToEnd();
                    xml.LoadXml(content);
                    reader.Close();
                }

                if (!String.IsNullOrEmpty(content))
                {
                    CustomLogger.LogMessage(content);
                    XmlNodeList rootXML = xml.GetElementsByTagName("NotifyScreeningPackageOrder");

                    if (rootXML.Count > 0 && rootXML.Item(0).HasChildNodes)
                    {
                        XmlElement reader = (XmlElement)rootXML.Item(0);
                        String ScreeningSubjectID = Utils.getXMLValue(reader, "ScreeningSubjectID");
                        CustomLogger.LogMessage("ScreeningSubjectID: " + ScreeningSubjectID);
                        String[] tokens = ScreeningSubjectID.Split('-');
                        String endpoint = "";
                        String alternateEndpoint = "";

                        if (tokens[0] == "dev5")
                        {
                            endpoint            = "https://adev5hocdev5-developer-edition.na37.force.com/services/apexrest/HOCdev5/NotifyScreeningPackageOrder";
                            alternateEndpoint   = "https://adev5hocdev5-developer-edition.na37.force.com/services/apexrest/HOCdev5/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9999vv")
                        {
                            endpoint            = "https://vv-hocdemo.cs50.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint   = "http://vv-hocdemo.cs50.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0034")
                        {
                            endpoint            = "https://handsonnashville.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint   = "http://handsonnashville.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0077")
                        {
                            endpoint            = "https://handsonnwnc.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint   = "http://handsonnwnc.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        /*if (tokens[0] == "0139")
                        {
                            endpoint            = "https://metrovolunteers.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint   = "http://metrovolunteers.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }*/
                        if (tokens[0] == "0139")
                        {
                            endpoint            = "https://sparkthechangecolorado.my.salesforce.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint   = "https://sparkthechangecolorado.my.salesforce.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9999")
                        {
                            endpoint = "https://hocdemo.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://hocdemo.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9001_a")
                        {
                            endpoint = "https://ps0dev-trial-hoc-v1-12e7242a94d.cs63.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://ps0dev-trial-hoc-v1-12e7242a94d.cs63.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9001")
                        {
                            endpoint = "https://trial-hoc-v1-12e7242a94d.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://trial-hoc-v1-12e7242a94d.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0208")
                        {
                            endpoint = "http://irusa.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://irusa.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0208_a")
                        {
                            endpoint = "http://hoc-irusa.cs26.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://hoc-irusa.cs26.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0088")
                        {
                            endpoint = "https://volhamptonroads.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://volhamptonroads.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0180")
                        {
                            endpoint = "https://nfte.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://nfte.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9030")
                        {
                            endpoint = "http://trial-hoc-v1-15637e4f89f.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "https://trial-hoc-v1-15637e4f89f.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0019")
                        {
                            endpoint = "https://handsongr.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://handsongr.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "0225")
                        {
                            endpoint = "https://capitalareafoodbank.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://capitalareafoodbank.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9015")
                        {
                            endpoint = "https://trial-hoc-v1-15c12cf05b7-1607a9d6fbb.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://trial-hoc-v1-15c12cf05b7-1607a9d6fbb.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9016")
                        {
                            endpoint = "https://trial-hoc-v1-15c12cf05b7-1607aa0309e.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://trial-hoc-v1-15c12cf05b7-1607aa0309e.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }
                        if (tokens[0] == "9017")
                        {
                            endpoint = "https://trial-hoc-v1-15c12cf05b7-1607f12bfe2.secure.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                            alternateEndpoint = "http://trial-hoc-v1-15c12cf05b7-1607f12bfe2.force.com/services/apexrest/HOC/NotifyScreeningPackageOrder";
                        }

                        if (!string.IsNullOrEmpty(endpoint))
                        {
                            HttpResponseMessage response = redirectRequestToSalesforce(endpoint, content);
                            if (!response.IsSuccessStatusCode)
                            {
                                if (!string.IsNullOrEmpty(alternateEndpoint))
                                {
                                    response = redirectRequestToSalesforce(alternateEndpoint, content);

                                    if (!response.IsSuccessStatusCode)
                                    {
                                        CustomLogger.LogMessage("502 Bad Gateway. The salesforce instance do not respond.", GlobalEnums.LogType.Error);
                                        context.Response.StatusCode = 502;
                                        context.Response.Status = "502 Bad Gateway. The salesforce instance do not respond.";
                                        context.Response.Write(Utils.getResponse("Bad Gateway"));
                                    }else
                                    {
                                        context.Response.StatusCode = 201;
                                        context.Response.Status = "201 Created";
                                        context.Response.Write(Utils.getResponse("Ok"));
                                        CustomLogger.LogMessage("Request redirected to Salesforce instance. " + tokens[0]);
                                    }
                                }else
                                {
                                    CustomLogger.LogMessage("502 Bad Gateway. The salesforce instance do not respond.", GlobalEnums.LogType.Error);
                                    context.Response.StatusCode = 502;
                                    context.Response.Status = "502 Bad Gateway. The salesforce instance do not respond.";
                                    context.Response.Write(Utils.getResponse("Bad Gateway"));
                                }
                            }else
                            {
                                context.Response.StatusCode = 201;
                                context.Response.Status = "201 Created";
                                context.Response.Write(Utils.getResponse("Ok"));
                                CustomLogger.LogMessage("Request redirected to Salesforce instance. " + tokens[0]);
                            }
                        }
                    }
                    else
                    {
                        CustomLogger.LogMessage("Received unexpected XML file from Verified Volunteers", GlobalEnums.LogType.Error);
                        context.Response.StatusCode = 400;
                        context.Response.Status = "400 Bad request";
                        context.Response.Write(Utils.getResponse("Received unexpected XML file from Verified Volunteers"));
                        return;
                    }
                }
                else
                {
                    CustomLogger.LogMessage("There was an error processing the request from Verified Volunteers", GlobalEnums.LogType.Error);
                    context.Response.StatusCode = 400;
                    context.Response.Status = "400 Bad request";
                    context.Response.Write(Utils.getResponse("There was an error processing the request from Verified Volunteers"));
                    return;
                }
            }catch ( Exception ex) {
                CustomLogger.LogMessage("There was an error processing the request from Verified Volunteers.", GlobalEnums.LogType.Error);
                CustomLogger.LogException(ex, "Verified Volunteers");
                context.Response.StatusCode = 400;
                context.Response.Status = "400 Bad request";
                context.Response.Write(Utils.getResponse("There was an error processing the request from Verified Volunteers."));
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private HttpResponseMessage redirectRequestToSalesforce(string URI, string data)
        {
            HttpResponseMessage response = null;
            try
            {
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URI);
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/xml"));
                StringContent content = new StringContent(data);
                response = client.PostAsync(URI, content).Result;
            }
            catch (Exception e)
            {
                CustomLogger.LogMessage("There was an error making the request to salesforce instance.", GlobalEnums.LogType.Error);
                CustomLogger.LogException(e, "Request to Salesforce");
            }
            return response;
        }


        // TODO: Here is where you would validate the username and password.
        private static bool CheckPassword(string username, string password)
        {
            return username == "VVHOCTest" && password == "Verified1!";
        }

        private static Boolean AuthenticateUser(string credentials)
        {
            try
            {
                var encoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                credentials = encoding.GetString(Convert.FromBase64String(credentials));

                int separator = credentials.IndexOf(':');
                string name = credentials.Substring(0, separator);
                string password = credentials.Substring(separator + 1);

                if (CheckPassword(name, password))
                {
                    return true;
                }
                else
                {
                    // Invalid username or password.
                    return false;
                }
            }
            catch (FormatException)
            {
                // Credentials were not formatted correctly.
                return false;
            }
        }

        private static Boolean OnApplicationAuthenticateRequest(HttpRequest Request)
        {
            //var request = HttpContext.Current.Request;
            var authHeader = Request.Headers["Authorization"];
            if (authHeader != null)
            {
                var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);

                // RFC 2617 sec 1.2, "scheme" name is case-insensitive
                if (authHeaderVal.Scheme.Equals("basic",
                        StringComparison.OrdinalIgnoreCase) &&
                    authHeaderVal.Parameter != null)
                {
                    return AuthenticateUser(authHeaderVal.Parameter);
                }
            }
            return false;
        }

    }
}
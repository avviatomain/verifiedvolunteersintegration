﻿using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace VerifiedVolunteersIntegration.utils
{
    public static class CustomLogger
    {
        private static Logger log = LogManager.GetCurrentClassLogger();

        public static void LogMessage(string exTitle, GlobalEnums.LogType logType = GlobalEnums.LogType.Info)
        {
            switch (logType)
            {
                case GlobalEnums.LogType.Debug:
                    if ((int)GlobalEnums.LogType.Debug >= GetMinimumLogLevel())
                        log.Debug(exTitle);
                    break;
                case GlobalEnums.LogType.Info:
                    if ((int)GlobalEnums.LogType.Info >= GetMinimumLogLevel())
                        log.Info(exTitle);
                    break;
                case GlobalEnums.LogType.Warn:
                    if ((int)GlobalEnums.LogType.Warn >= GetMinimumLogLevel())
                        log.Warn(exTitle);
                    break;
                case GlobalEnums.LogType.Error:
                    if ((int)GlobalEnums.LogType.Error >= GetMinimumLogLevel())
                        log.Error(exTitle);
                    break;
                case GlobalEnums.LogType.Fatal:
                    if ((int)GlobalEnums.LogType.Fatal >= GetMinimumLogLevel())
                        log.Fatal(exTitle);
                    break;
            }

        }
        public static void LogException(Exception ex, string exTitle, GlobalEnums.LogType logType = GlobalEnums.LogType.Error)
        {
            string entry = exTitle + ": " + ex.ToString();

            switch (logType)
            {
                case GlobalEnums.LogType.Debug:
                    log.Debug(entry);
                    break;
                case GlobalEnums.LogType.Info:
                    log.Info(entry);
                    break;
                case GlobalEnums.LogType.Warn:
                    log.Warn(entry);
                    break;
                case GlobalEnums.LogType.Error:
                    log.Error(entry);
                    break;
                case GlobalEnums.LogType.Fatal:
                    log.Fatal(entry);
                    break;
            }
        }

        private static int GetMinimumLogLevel()
        {
            int minimumLevel = (int)GlobalEnums.LogType.Debug;

            string value = ConfigurationManager.AppSettings["MINIMUM_LOG_LEVEL"];
            Int32.TryParse(value, out minimumLevel);

            return minimumLevel;
        }
    }
}
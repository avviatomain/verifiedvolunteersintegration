﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VerifiedVolunteersIntegration.utils
{
    public class GlobalEnums
    {
        public enum LogType
        {
            Debug = 0,
            Info = 1,
            Warn = 2,
            Error = 3,
            Fatal = 4
        }

        public enum DynamicFilterStatus
        {
            Active = 1,
            Inactive = 2
        }
    }
}
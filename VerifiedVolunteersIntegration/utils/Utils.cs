﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;


namespace VerifiedVolunteersIntegration.utils
{
    public class Utils
    {
        public static string getXMLValue(XmlElement xmlElement, string name)
        {
            XmlNodeList Elements = xmlElement.GetElementsByTagName(name);

            if (Elements.Count > 0)
            {
                return Elements[0].InnerText;
            }
            return null;
        }

        public static String getResponse(String response)
        {
            String responseBase = "<element name=\"notificationResponse\">" +
                                    "<complexType>" +
                                        "<sequence>" +
                                            "<Ack>{response}</Ack>" +
                                         "</sequence >" +
                                     "</complexType >" +
                                   "</element >";

            return responseBase.Replace("{response}", response);
        }
    }
}